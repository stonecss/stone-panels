(function(Drupal, location) {

/**
 * AJAX framework command: panels ipe refresh.
 */
Drupal.ajax.prototype.commands.panels_ipe_refresh = function(ajax, response, status) {
  location.reload();
}

})(Drupal, location)
