<?php

/**
 * @file
 *
 * Theming functions for the Core module.
 */

/**
 * Returns HTML to wrap child elements in a container .
 *
 * Used as a #theme_wrapper for any renderable element.
 *
 * @param $variables
 *   An associative array containing:
 *   - container: An associative array containing the properties of the container.
 *     Properties used: #container_attributes, #children.
 *
 * @ingroup themeable
 */
function theme_stone_container($variables) {
  $attributes = array_merge_recursive(
    array('class' => array('Container')),
    $variables['container_attributes']
  );

  return '<' . $variables['container_element'] . drupal_attributes($attributes) . '>' .
           '<div class="Container-inner">' . $variables['children'] . '</div>' .
         '</' . $variables['container_element'] . '>';
}

/**
 * Returns HTML to wrap child elements in a grid .
 *
 * Used as a #theme_wrapper for any renderable element.
 *
 * @param $variables
 *   An associative array containing:
 *   - grid: An associative array containing the properties of the grid.
 *     Properties used: #grid_attributes, #children.
 *
 * @ingroup themeable
 */
function theme_stone_grid($variables) {
  $attributes = array_merge_recursive(
    array('class' => array('Grid')),
    $variables['grid_attributes']
  );

  return '<div' . drupal_attributes($attributes) . '>' . $variables['children'] . '</div>';
}

/**
 * Returns HTML to wrap child elements in a grid cell.
 *
 * Used as a #theme_wrapper for any renderable element.
 *
 * @param $variables
 *   An associative array containing:
 *   - grid_cell: An associative array containing the properties of the grid cell.
 *     Properties used: #grid_cell_attributes, #children.
 *
 * @ingroup themeable
 */
function theme_stone_grid_cell($variables) {
  $attributes = array_merge_recursive(
    array('class' => array('Grid-cell')),
    $variables['grid_cell_attributes']
  );

  return '<div' . drupal_attributes($attributes) . '>' . $variables['children'] . '</div>';
}

/**
 * Renders a stone layout.
 *
 * $variables['stone_layout'] is defined in template_preprocess_stone_layout()
 * and should be a drupal render array.
 */
function theme_stone_page_layout(&$variables) {
  return drupal_render($variables['stone_layout']);
}

/**
 * Implements hook_preprocess_stone_layout().
 */
function template_preprocess_stone_page_layout(&$variables) {
  $variables['stone_layout'] = stone_panels_get_stone_layout($variables);
}

/**
 * Renders a stone layout.
 *
 * $variables['stone_layout'] is defined in template_preprocess_stone_layout()
 * and should be a drupal render array.
 */
function theme_stone_site_layout(&$variables) {
  return drupal_render($variables['stone_layout']);
}

/**
 * Implements hook_preprocess_stone_layout().
 */
function template_preprocess_stone_site_layout(&$variables) {
  $variables['stone_layout'] = stone_panels_get_stone_layout($variables);
}

/**
 * Checks to see if stone_layout function has been defined for the layout being
 * rendered and stores the return value in $variables so it can be rendered in
 * theme_stone_layout(). stone layout functions should be defined in the form
 * stone_layout_LAYOUT_NAME() and should return a drupal render array.
 */
function stone_panels_get_stone_layout($variables) {
  $stone_layout = '';
  $layout_function = 'stone_layout_' . $variables['layout']['name'];
  if (function_exists($layout_function)) {
    $stone_layout = call_user_func_array($layout_function, array('variables' => $variables));
  }
  return $stone_layout;
}

/**
 * Preprocess variables for pane-content-header.tpl.php.
 */
function template_preprocess_pane_content_header(&$variables) {
  $variables['messages'] = theme('status_messages');
  $variables['title'] = drupal_get_title();
  $variables['title_attributes_array']['class'][] = 'PageTitle';
  $variables['tabs'] = menu_local_tabs();
  $variables['action_links'] = menu_local_actions();
  $variables['help'] = theme('help');

  // When page title is set to hide, make it invisible.
  // More accessible for screen readers and SEO.
  if (($node = menu_get_object()) && !empty($node->field_hide_title[LANGUAGE_NONE][0]['value'])) {
    $variables['title_attributes_array']['class'][] = 'u-hiddenVisually';
  }
}
