<?php

/**
 * Implements hook_default_panels_renderer_pipeline().
 */
function stone_panels_default_panels_renderer_pipeline() {
  $pipelines = array();

  $pipeline = new stdClass;
  $pipeline->disabled = FALSE; /* Edit this to true to make a default pipeline disabled initially */
  $pipeline->api_version = 1;
  $pipeline->name = 'ipe_restricted';
  $pipeline->admin_title = t('In-Place Editor: Restricted');
  $pipeline->admin_description = t('Allows privileged users to update and rearrange the content
    while viewing this panel, with restrictions in place on the content panes that can be added.');
  $pipeline->weight = 0;
  $pipeline->settings = array(
    'renderers' => array(
      0 => array(
        'access' => array(
          'plugins' => array(
            0 => array(
              'name' => 'perm',
              'settings' => array(
                'perm' => 'use panels in place editing',
              ),
              'context' => 'logged-in-user',
            ),
          ),
          'logic' => 'and',
        ),
        'renderer' => 'ipe_restricted',
        'options' => array(),
      ),
    ),
  );
  $pipelines[$pipeline->name] = $pipeline;

  return $pipelines;
}
