<?php

/**
 * Renderer class for all In-Place Editor (IPE) behavior.
 */
class panels_renderer_ipe_restricted extends panels_renderer_ipe {
  /**
   * Create a list of categories from all of the content type.
   *
   * @return array
   *   An array of categories. Each entry in the array will also be an array
   *   with 'title' as the printable title of the category, and 'content'
   *   being an array of all content in the category. Each item in the 'content'
   *   array contain the array plugin definition so that it can be later
   *   found in the content array. They will be keyed by the title so that they
   *   can be sorted.
   */
  function get_categories($content_types) {
    $output = parent::get_categories($content_types);

    // Can't see another plausible way of doing this...
    variable_set('stone_panels_panel_pane_categories', $output);

    if ($amendments = variable_get('stone_panels_settings_page_form_values', FALSE)) {
      foreach ($output as $key => $category) {
        // Rename specified categories
        if (!empty($amendments["rename-{$key}"])) {
          $output[$key]['title'] = $amendments["rename-{$key}"];
        }

        // Remove specified panes
        foreach ($amendments["panel_panes__panel_category_fieldset__{$key}"] as $pane) {
          if (!empty($pane)) {
            unset($output[$key]['content'][$pane]);
          }
        }

        // Remove specified panes
        if (!empty($amendments['categories'][$key])) {
          unset($output[$key]);
        }
      }
    }

    return $output;
  }

  /**
   * AJAX entry point to create the controller form for an IPE.
   */
  function ajax_save_form($break = NULL) {
    parent::ajax_save_form($break);

    if ($this->meta_location == 'inline') {
      $this->commands[] = stone_panels_panels_ipe_refresh_command();
    }
  }

  /**
   * AJAX entry point to create the controller form for an IPE.
   */
  function ajax_set_layout($layout) {
    parent::ajax_set_layout($layout);

    if ($this->meta_location == 'inline') {
      $this->commands[] = stone_panels_panels_ipe_refresh_command();
    }
  }
}
