<?php

// Plugin definition
$plugin = array(
  'title' => t('2 Columns Stacked: 66 - 33'),
  'category' => t('2 Columns'),
  'icon' => 'default.png',
  'theme' => 'stone_page_layout',
  'regions' => array(
    'top' => t('Top'),
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_2_columns_stacked_66_33($variables) {
  return array(
    'top' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--top'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'top' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['top'],
      ),
    ),
    'main' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size2of3', 'u-lg-size2of3')),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size1of3', 'u-lg-size1of3')),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
    'bottom' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--bottom'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'bottom' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['bottom'],
      ),
    ),
  );
}
