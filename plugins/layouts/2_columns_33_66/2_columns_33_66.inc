<?php

// Plugin definition
$plugin = array(
  'title' => t('2 Columns: 33 - 66'),
  'category' => t('2 Columns'),
  'icon' => 'default.png',
  'theme' => 'stone_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_2_columns_33_66($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size2of3', 'u-lg-size2of3', 'u-md-push1of3', 'u-lg-push1of3')
        ),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size1of3', 'u-lg-size1of3', 'u-md-pull2of3', 'u-lg-pull2of3')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
  );
}
