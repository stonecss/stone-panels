<?php

// Plugin definition
$plugin = array(
  'title' => t('3 Columns Stacked: 20 - 20 - 60'),
  'category' => t('3 Columns'),
  'icon' => 'default.png',
  'theme' => 'stone_page_layout',
  'regions' => array(
    'top' => t('Top'),
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_3_columns_stacked_20_20_60($variables) {
  return array(
    'top' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--top'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'top' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['top'],
      ),
    ),
    'main' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size2of4', 'u-lg-size3of5', 'u-md-push2of4', 'u-lg-push2of5')
        ),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size1of4', 'u-lg-size1of5', 'u-md-pull2of4', 'u-lg-pull3of5')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
      'tertiary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size1of4', 'u-lg-size1of5', 'u-md-pull2of4', 'u-lg-pull3of5')
        ),
        '#markup' => $variables['content']['tertiary'],
      ),
    ),
    'bottom' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--bottom'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'bottom' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['bottom'],
      ),
    ),
  );
}
