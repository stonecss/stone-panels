<?php

// Plugin definition
$plugin = array(
  'title' => t('4 Columns: 25 - 25 - 25 - 25'),
  'category' => t('4 Columns'),
  'icon' => 'default.png',
  'theme' => 'stone_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'quaternary' => t('Quaternary'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_4_columns_25_25_25_25($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size2of4', 'u-lg-size1of4')),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size2of4', 'u-lg-size1of4')),
        '#markup' => $variables['content']['secondary'],
      ),
      'tertiary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size2of4', 'u-lg-size1of4')),
        '#markup' => $variables['content']['tertiary'],
      ),
      'quaternary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array('class' => array('u-md-size2of4', 'u-lg-size1of4')),
        '#markup' => $variables['content']['quaternary'],
      ),
    ),
  );
}
