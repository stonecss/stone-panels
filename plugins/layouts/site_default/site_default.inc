<?php

// Plugin definition
$plugin = array(
  'title' => t('Site default'),
  'category' => t('Site'),
  'icon' => 'site_default.png',
  'theme' => 'stone_site_layout',
  'regions' => array(
    'header_top' => t('Header top'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_site_default($variables) {
  $layout = array(
    'header_top_container' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--headerTop'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'header_top' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['header_top'],
      ),
    ),
    'header_container' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'header',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--header'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'header' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['header'],
      ),
    ),
    'navigation_container' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--navigation'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'navigation' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['navigation'],
      ),
    ),
    'main' => array(
      '#markup' => $variables['content']['content'],
    ),
    'footer_container' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'footer',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--footer'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'footer' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['footer'],
      ),
    ),
    'footer_bottom_container' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--footerBottom'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'footer_bottom' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#markup' => $variables['content']['footer_bottom'],
      ),
    ),
  );

  // If current page isn't handled by page manager wrap content in a container.
  $current_page = page_manager_get_current_page();
  if (empty($current_page)) {
    $layout['main'] = array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'main' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        'content_header' => array(
          '#theme' => 'pane_content_header'
        ),
        'content' => array(
          '#markup' => $variables['content']['content'],
        ),
      ),
    );
  }

  return $layout;
}
