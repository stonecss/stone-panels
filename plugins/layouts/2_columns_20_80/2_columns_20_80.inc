<?php

// Plugin definition
$plugin = array(
  'title' => t('2 Columns: 20 - 80'),
  'category' => t('2 Columns'),
  'icon' => 'default.png',
  'theme' => 'stone_page_layout',
  'regions' => array(
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
  ),
);

/**
 * Implements stone_layout_LAYOUT_NAME().
 */
function stone_layout_2_columns_20_80($variables) {
  return array(
    'main' => array(
      '#theme_wrappers' => array('stone_grid', 'stone_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
      ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutter'),
      ),
      'primary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size3of4', 'u-lg-size4of5', 'u-md-push1of4', 'u-lg-push1of5')
        ),
        '#markup' => $variables['content']['primary'],
      ),
      'secondary' => array(
        '#theme_wrappers' => array('stone_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-md-size1of4', 'u-lg-size1of5', 'u-md-pull3of4', 'u-lg-pull4of5')
        ),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
  );
}
