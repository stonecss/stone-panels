<?php
/**
 * @file
 * stone_panels.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function stone_panels_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_node_view_disabled';
  $strongarm->value = FALSE;
  $export['page_manager_node_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_term_view_disabled';
  $strongarm->value = FALSE;
  $export['page_manager_term_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panels_everywhere_site_template_enabled';
  $strongarm->value = 1;
  $export['panels_everywhere_site_template_enabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panels_page_allowed_layouts';
  $strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:11:"panels_page";s:23:"allowed_layout_settings";a:11:{s:8:"flexible";b:0;s:14:"twocol_stacked";b:0;s:13:"twocol_bricks";b:0;s:6:"twocol";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:17:"threecol_33_34_33";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:17:"threecol_25_50_25";b:0;s:6:"onecol";b:0;s:12:"site_default";b:1;s:12:"page_default";b:1;}s:10:"form_state";N;}';
  $export['panels_page_allowed_layouts'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'stone_panels_settings_page_form_values';
  $strongarm->value = array(
    'categories' => array(
      'activity' => 'activity',
      'entity' => 'entity',
      'form' => 'form',
      'node' => 'node',
      'node--tokens-' => 'node--tokens-',
      'page-elements' => 'page-elements',
      'widgets' => 'widgets',
      'custom' => 0,
      'existing-pages' => 0,
      'lists' => 0,
      'menus' => 0,
      'miscellaneous' => 0,
      'promotions' => 0,
      'root' => 0,
    ),
    'rename-activity' => '',
    'rename-custom' => '',
    'rename-entity' => '',
    'rename-existing-pages' => '',
    'rename-form' => '',
    'rename-lists' => '',
    'rename-menus' => '',
    'rename-miscellaneous' => 'Blocks',
    'rename-node' => '',
    'rename-node--tokens-' => '',
    'rename-page-elements' => '',
    'rename-promotions' => '',
    'rename-root' => '',
    'rename-widgets' => '',
    'panel_panes__panel_category_fieldset__activity' => array(
      'Who\'s new' => 0,
      'Who\'s online' => 0,
    ),
    'panel_panes__panel_category_fieldset__custom' => array(
      'Search form' => 0,
    ),
    'panel_panes__panel_category_fieldset__entity' => array(
      'Rendered Node' => 0,
    ),
    'panel_panes__panel_category_fieldset__existing-pages' => array(
      'Existing page' => 0,
    ),
    'panel_panes__panel_category_fieldset__form' => array(
      'Field form: Body' => 0,
      'Field form: Related content' => 0,
      'Field form: Related documents' => 0,
      'Field form: Related substances' => 0,
      'Field form: Title' => 0,
      'Field form: Hide title' => 0,
    ),
    'panel_panes__panel_category_fieldset__lists' => array(
      'Blog Articles' => 0,
      'Case studies' => 0,
      'Documents' => 0,
      'News' => 0,
      'Vacancies' => 0,
    ),
    'panel_panes__panel_category_fieldset__menus' => array(
      'Development' => 'Development',
      'Footer menu' => 'Footer menu',
      'Useful links' => 'Useful links',
      'User menu: level 1' => 'User menu: level 1',
      'Main menu: level 1' => 'Main menu: level 1',
      'Main menu: levels 1-3 (dropdown)' => 'Main menu: levels 1-3 (dropdown)',
      'Footer menu: level 1' => 'Footer menu: level 1',
      'Navigation' => 'Navigation',
      'Management' => 'Management',
      'User menu' => 'User menu',
      'Main menu' => 'Main menu',
      'menu tree of <em class="placeholder">the menu selected by the page</em>' => 'menu tree of <em class="placeholder">the menu selected by the page</em>',
      '<em class="placeholder">Development</em> menu tree' => '<em class="placeholder">Development</em> menu tree',
      '<em class="placeholder">Footer menu</em> menu tree' => '<em class="placeholder">Footer menu</em> menu tree',
      '<em class="placeholder">Main menu</em> menu tree' => '<em class="placeholder">Main menu</em> menu tree',
      '<em class="placeholder">Management</em> menu tree' => '<em class="placeholder">Management</em> menu tree',
      '<em class="placeholder">Navigation</em> menu tree' => '<em class="placeholder">Navigation</em> menu tree',
      '<em class="placeholder">Useful links</em> menu tree' => '<em class="placeholder">Useful links</em> menu tree',
      '<em class="placeholder">User menu</em> menu tree' => '<em class="placeholder">User menu</em> menu tree',
      'Section: about us (menu)' => 0,
      'User menu: levels 2-5' => 0,
      'Main menu: levels 2-5' => 0,
      'Footer menu: levels 2-5' => 0,
    ),
    'panel_panes__panel_category_fieldset__miscellaneous' => array(
      'Inline differences' => 'Inline differences',
      'Copyright' => 'Copyright',
      'Execute PHP' => 'Execute PHP',
      'Switch user' => 'Switch user',
      'Node subpage menu' => 0,
      'Related documents' => 0,
      'Related substances' => 0,
      'Related content' => 0,
      'CRI: Search Results' => 0,
      'Services: Choose service form' => 0,
      'Services: List' => 0,
      'Services: Search form' => 0,
      'Services: Simple search form' => 0,
      'Services: Search map' => 0,
      'Services: Search list' => 0,
      'Services: Contact' => 0,
      'OffCanvas Handle' => 0,
      'Social Networks' => 0,
      'Flickr' => 0,
      'Facebook Feed' => 0,
      'Taxonomy Menu Block (News Article Types (categories))' => 0,
    ),
    'panel_panes__panel_category_fieldset__node' => array(
      'Attached files' => 0,
      'Node title' => 0,
      'Node content' => 0,
      'Node terms' => 0,
      'Node author' => 0,
      'Node last updated date' => 0,
      'Node created date' => 0,
      'Node body' => 0,
      'Node type description' => 0,
      'Node links' => 0,
      'Field: Body (body)' => 0,
      'Field: Related content (field_related_content)' => 0,
      'Field: Related documents (field_related_documents)' => 0,
      'Field: Related substances (field_related_substances)' => 0,
      'Field: Title (title_field)' => 0,
      'Field: Hide title (field_hide_title)' => 0,
    ),
    'panel_panes__panel_category_fieldset__node--tokens-' => array(
      'Translation source node' => 0,
      'Revision log message' => 0,
      'Content type' => 0,
      'Latest differences' => 0,
      'Latest differences (marked down)' => 0,
      'Menu link' => 0,
      'Metatag.' => 0,
      'Content ID' => 0,
      'Revision ID' => 0,
      'Title' => 0,
      'Body' => 0,
      'Summary' => 0,
      'Language' => 0,
      'URL' => 0,
      'Edit URL' => 0,
      'Date created' => 0,
      'Date changed' => 0,
      'Author' => 0,
      'Publish on date' => 0,
      'Unpublish on date' => 0,
      'Original node' => 0,
      'Title-' => 0,
      'Hide title' => 0,
      'Related content' => 0,
      'Related documents' => 0,
      'Related substances' => 0,
      'Image' => 0,
      'File' => 0,
      'Image-' => 0,
      'Type' => 0,
      'Type-' => 0,
      'Salary' => 0,
      'Closing Date' => 0,
      'Hours' => 0,
      'How to Apply' => 0,
      'Job Specification' => 0,
      'Job Location' => 0,
      'Service' => 0,
      'Address' => 0,
      'Email' => 0,
      'Fax' => 0,
      'Geoservice' => 0,
      'Images' => 0,
      'Satellite Services' => 0,
      'Image--' => 0,
      'Message' => 0,
      'Name' => 0,
      'User' => 0,
      'Facebook' => 0,
      'Twitter' => 0,
      'Telephone' => 0,
      'Substance Type' => 0,
      'Synonyms' => 0,
      'Address-' => 0,
      'Application Form' => 0,
      'Job Reference' => 0,
      'Service manager email' => 0,
      'Project code' => 0,
      'Regional director email' => 0,
      'Intro &amp; how to find us' => 0,
      'Image---' => 0,
      'Geoservice-' => 0,
      'Opening hours &amp; weekly timetable' => 0,
      'Telephone (other)' => 0,
      'Geoservice--' => 0,
    ),
    'panel_panes__panel_category_fieldset__page-elements' => array(
      'System help' => 0,
      'Breadcrumb' => 0,
      'Help' => 0,
      'Site slogan' => 0,
      'Site name' => 0,
      'Tabs' => 0,
      'Page title' => 0,
      'Secondary navigation links' => 0,
      'Feed icons' => 0,
      'Status messages' => 0,
      'Actions' => 0,
      'Primary navigation links' => 0,
      'Site logo' => 0,
      'Page messages' => 0,
      'Page header' => 0,
      'Page navigation' => 0,
      'Page content header' => 0,
    ),
    'panel_panes__panel_category_fieldset__promotions' => array(
      'Facts and Figures: Staff' => 0,
    ),
    'panel_panes__panel_category_fieldset__root' => array(
      'Existing node' => 'Existing node',
      'New custom content' => 0,
    ),
    'panel_panes__panel_category_fieldset__widgets' => array(
      'Language switcher (User interface text)' => 0,
      'Language switcher (Content)' => 0,
      'Syndicate' => 0,
      'Recent content' => 0,
      'Powered by Drupal' => 0,
      'User login' => 0,
    ),
    'submit' => 'Save',
    'form_build_id' => 'form-G-r7Z1JAbecAuwRofauy9GhYk8gDY9-mR5cjZBZbt8Q',
    'form_token' => 'Q7QlSh_pz76ujw9xO4_qJK1ZPnInQ1KabKdhcm7oIYg',
    'form_id' => 'stone_panels_settings_page_form',
    'op' => 'Save',
  );
  $export['stone_panels_settings_page_form_values'] = $strongarm;

  return $export;
}
