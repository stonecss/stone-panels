<?php
/**
 * @file
 * stone_panels.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stone_panels_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == 'panels' && $api == 'pipelines') {
    return array(
      'version' => 1,
      'path' => drupal_get_path('module', 'stone_panels') . '/includes',
    );
  }

}
